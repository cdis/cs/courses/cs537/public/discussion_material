# Discussion 06

## xv6 basics

- The xv6 operating system is present in the `~cs537-1/xv6/` directory on CSL AFS.
- untar it
  - $ `tar -xzf ~cs537-1/xv6/xv6.tar.gz -C .`
- run xv6
  - $ `make qemu-nox`
- Exit xv6
  - using `Ctrl-a  x`

## Project 04 specifics

Change the `Makefile`

- `CFLAGS` from `-O2` to `-O0`
- Ensure that `CPUS := 1`

## GDB

$ `echo "set auto-load safe-path /" >> ~/.gdbinit` [do it only the first time]

$ `make qemu-nox-gdb` (in one terminal)

$ `gdb` (split the terminal and run in another terminal)

$ `continue`

`CTRL + C` to halt gdb

$ `break main`

$ `next`

etc.

## Trace scheduling in xv6

### main.c

`main()` calls

- `userinit()` // first user process
  - create other processes by fork
- `mpmain()`  // finish this processor's setup
  - calls `scheduler()` // start running processes

### proc.c

`scheduler()` runs on every tick.

inside `scheduler()`

- *infinite* for loop
- locks are important, they wrap important pieces of codes to avoid race condition (more on that in project 06)
- `struct proc *p` (inside `proc.h`)
  - any info you want to track about a particular process should go here.

    ```
    enum procstate { UNUSED, EMBRYO, SLEEPING, RUNNABLE, RUNNING, ZOMBIE };
    ```
    ```
    // Per-process state
    struct proc {
        uint sz;                     // Size of process memory (bytes)
        pde_t* pgdir;                // Page table
        char *kstack;                // Bottom of kernel stack for this process
        enum procstate state;        // Process state
        int pid;                     // Process ID
        struct proc *parent;         // Parent process
        struct trapframe *tf;        // Trap frame for current syscall
        struct context *context;     // swtch() here to run process
        void *chan;                  // If non-zero, sleeping on chan
        int killed;                  // If non-zero, have been killed
        struct file *ofile[NOFILE];  // Open files
        struct inode *cwd;           // Current directory
        char name[16];               // Process name (debugging)
    };
    ```

- `#define NPROC        64` (inside `param.h`)
  - // maximum number of processes
- `switchuvm()` (inside `vm.c`)
  - // Switch TSS and h/w page table to correspond to process p
- `swtch(&(c->scheduler), p->context)` (inside `swtch.S`)
  - // Save the current registers on the stack, creating a struct context, and save its address in *old. Switch stacks to new and pop previously-saved registers.
  - From this point on, the process `p` starts running,
  - The next line `switchkvm` will run after a timer interrupt, using the flow `yield()` --> `sched()` --> `swtch(&p->context, mycpu()->scheduler)`

- `switchkvm()` (inside `vm.c`)
  - // Switch h/w page table register to the kernel-only page table, for when no process is running.

### `trap.c`

`trap()`

- `case T_IRQ0 + IRQ_TIMER:`
  - calls `wakeup(&ticks);`  (inside `proc.c`)
  - calls `yield();` (inside `proc.c`)

### proc.c again

`yield()`

- sets curr proc from RUNNING to RUNNABLE `myproc()->state = RUNNABLE;`
- calls `sched()` (inside `proc.c`)

`sched()`

- calls `swtch(&p->context, mycpu()->scheduler)`
  - here context switches from user process to kernel mode and resumes the scheduler code it was running before

`wakeup()`

- calls `wakeup1` (inside `proc.c`)

`wakeup1()`

- wakes up all sleeping processes

  ```
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
          if(p->state == SLEEPING && p->chan == chan)
              p->state = RUNNABLE;
  ```

### Now, let's investigate the `sleep()` syscall a bit

`sys_sleep()` (inside `sysproc.c`)

- calls `sleep()` in a loop

`sleep()` (inside `proc.c`)

- changes state to SLEEPING
- calls `sched()` which in turn calls `swtch(&p->context, mycpu()->scheduler)`


## Examples

- about-to-run0.png

- about-to-run-spin0.png
