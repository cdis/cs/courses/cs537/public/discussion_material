#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>

//Look at the man pages for opendir, readdir
//Pay attention to the dirent struct described in the readdir page
//You may also want to check out
//https://www.gnu.org/software/libc/manual/html_node/Testing-File-Type.html
//To see how to test for different file types (directories, regular files, etc.)
int main(int argc, char * argv[]) {
	char * dirToRead="./";
	DIR *dir;
	struct dirent *entry;
	dir = opendir(dirToRead);
	if (dir == NULL) {
		printf("Error opening directory");
		return 0;
	}
	while ((entry = readdir(dir)) != NULL) {
		char *filename = entry->d_name;
		char fullFilename[256];
		sprintf(fullFilename,"%s%s",dirToRead,filename);
		struct stat fileStat;
		int result = stat(fullFilename,&fileStat);
		if (result != 0 || S_ISREG(fileStat.st_mode)==0) {
			printf("%s is not a regular file\n",filename);
		} else {
			printf("%s regular file\n",filename);
		}
	}
}
