#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//man pages for fopen, fclose, fgets, and fprintf
//may want to check out https://www.geeksforgeeks.org/basics-file-handling-c/

int main(int argc, char * argv[]) {
  FILE * fp;
  char line[256];
  size_t len = 0;
  ssize_t read;
  
  fp = fopen("blah","w");
  
  fprintf(fp,"testing one two three\n");
  fprintf(fp,"here is another line of text\n");

  fclose(fp);



  fp = fopen("blah", "r");
  if (fp == NULL) {
    printf("failed in opening file\n");
    return 0;
  }
  
  while ((fgets(line, sizeof(line), fp)) != NULL) {
    printf("Retrieved line of length %ld:\n", strlen(line));
    printf("%s", line);
  }

  fclose(fp);
  return 0;
}
