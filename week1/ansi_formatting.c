#include <stdio.h>
#include <unistd.h>

//look at the SGR parameters table here: https://en.wikipedia.org/wiki/ANSI_escape_code#CSI_codes
//also look at answer 1 here: https://stackoverflow.com/questions/46180487/how-do-you-overlap-text-in-c-terminal

void printUnderline(char *text) {
  printf("\033[4m");
  printf("%s",text);
  printf("\033[0m");
}

void printBold(char *text) {
  printf("\033[1m");
  printf("%s",text);
  printf("\033[0m");
}

void printItalic(char *text) {
  printf("\033[3m");
  printf("%s",text);
  printf("\033[0m");
}

int main() {
  printUnderline("Hello");
  printf(", ");
  printBold("Bold");
  printItalic(" World!\n");
  printf("\033[1;31mHello, \033[4;32mColorful\033[0;1;37m World!\033[0m\n");
  return 0;
}
